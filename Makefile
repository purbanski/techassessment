CC = gcc
CFLAGS = -std=c99
OBJ = main.o test.o tritree.o utils.o test.o
APP = tritree

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

$(APP): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

memcheck: $(APP)
	@ which valgrind &> /dev/null && valgrind --leak-check=full ./$(APP) || echo -e "\nValgrind is not accessible. Can't perform memory leaks check.\n"

clean:
	rm -f *.o $(APP)
