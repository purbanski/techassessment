# Tech assessment #

### Problem description ###

Given a ternary tree, create its flatten representation (using DFS).

### Implementation ###

Although I'm really C++ programmer, and I haven't done C for many years, I decided to complete this task using C. The reason for that is following function has really C syntax.
```
#!c

value_t *flatten(TreeNode *n, size_t *num_elements);

```
I wasn't sure if it is allowed to change its signature so it returns vector (or use vector passed by reference). 

Traversal algorithm is implemented in the file: **tritree.c**

### Testing ###

Unit Tests have been created. Please refer to *test.c* and *utils.c* files. In order to build and run tests, please execute:

```
#!bash

$ make
$ ./tritree
```


Memory leaks test - I used *valgrind* (available on Linux) in order to check memory leaks. To run tests through valgrind please execute:


```
#!bash

$ make memcheck
```

### Assumptions and other ###

* Currently algorithm is capable of handling 100,000 data entries. In other words, buffor allocated for a flat tree data entries, can store up to 100,000 values. I could write routine for realloc if maximum is reached, however my understanding was this exercise is to test algorithm (and if it was C++, probably vector would be in use, and vector would handle its own resizing).

* Errors are checked using assert macro. As assert macro is only for debug builds, proper error handling mechanism should be considered (depending on language used).