//
//  utils.h
//  ARM-tech-assessment
//
//  Created by Przemek Urbanski on 24/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef utils_h
#define utils_h

#include <stdio.h>
#include "tritree.h"

/* 
 * Functions used to create tree nodes.
 */
TreeNode *node_create();
TreeNode *node_create_with_data(value_t left_value, value_t right_value);
TreeNode *node_create_with_left_data(value_t value);
TreeNode *node_create_with_right_data(value_t value);

/* 
 * Functions used to set relations between tree nodes.
 */
void node_set_left_child(TreeNode *parent, TreeNode *child);
void node_set_right_child(TreeNode *parent, TreeNode *child);
void node_set_mid_child(TreeNode *parent, TreeNode *child);

/*
 * Function deallocates memory used by tree.
 */
void tree_free(TreeNode *root);

#endif /* utils_h */
