//
//  utils.c
//  ARM-tech-assessment
//
//  Created by Przemek Urbanski on 24/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <stdlib.h>
#include <assert.h>
#include "utils.h"

DataNode *data_node_create(value_t value);

TreeNode *node_create() {
    TreeNode *node = (TreeNode*) malloc(sizeof(TreeNode));
    assert(node != NULL);
    
    node->left_tree = NULL;
    node->mid_tree = NULL;
    node->right_tree = NULL;
    node->left_data = NULL;
    node->right_data = NULL;
    
    return node;    
}

TreeNode *node_create_with_data(value_t left_value, value_t right_value) {
    TreeNode *node = node_create();
    node->left_data = data_node_create(left_value);
    node->right_data = data_node_create(right_value);
    
    return node;
}

TreeNode *node_create_with_left_data(value_t value) {
    TreeNode *node = node_create();
    node->left_data = data_node_create(value);
    
    return node;
}

TreeNode *node_create_with_right_data(value_t value) {
    TreeNode *node = node_create();
    node->right_data = data_node_create(value);
    
    return node;
}

void node_set_left_child(TreeNode *parent, TreeNode *child) {
    assert(parent !=  NULL);
    assert(parent->left_tree == NULL);
    assert(child !=  NULL);
    
    parent->left_tree = child;
}

void node_set_right_child(TreeNode *parent, TreeNode *child) {
    assert(parent !=  NULL);
    assert(parent->right_tree == NULL);
    assert(child !=  NULL);
    
    parent->right_tree = child;
}

void node_set_mid_child(TreeNode *parent, TreeNode *child) {
    assert(parent !=  NULL);
    assert(parent->mid_tree == NULL);
    assert(child !=  NULL);
    
    parent->mid_tree = child;
}

DataNode *data_node_create(value_t value) {
    DataNode *data_node = (DataNode*) malloc(sizeof(DataNode));
    assert(data_node != NULL);
    
    data_node->value = value;
    return data_node;
}

void tree_free(TreeNode *node) {
    if (node==NULL) {
        return;
    }
    
    if (node->left_data != NULL) {
        free(node->left_data);
    }

    if (node->right_data != NULL) {
        free(node->right_data);
    }

    tree_free(node->right_tree);
    tree_free(node->mid_tree);
    tree_free(node->left_tree);
    
    free(node);
}

