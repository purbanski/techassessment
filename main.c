//
//  main.c
//  ARM-tech-assessment
//
//  Created by Przemek Urbanski on 24/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include "test.h"

int main(int argc, const char * argv[]) {
    run_all_tests();
    
    return 0;
}
