//
//  test.c
//  ARM-tech-assessment
//
//  Created by Przemek Urbanski on 24/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "test.h"
#include "tritree.h"
#include "utils.h"

/*
 * Setting PRINT_TREE to true will cause
 * printing data values array, created on a test run.
 */
const bool PRINT_TREE = false;


/*
 * Function is common test routine. 
 */
void test_routine(TreeNode *root, size_t expected_data_count, const value_t *expected_tree, const char* fn_name) {
    
    bool ok = true;
    size_t tree_data_count;
    
    value_t *flat_tree = flatten(root, &tree_data_count);
    
    if (tree_data_count != expected_data_count) {
        ok = false;
        printf("%s: invalid data count [got:%lu ; expected:%lu]\n", fn_name, expected_data_count, tree_data_count);
    }
    
    for (int i=0; i<expected_data_count; i++) {
        if (flat_tree[i] != expected_tree[i]) {
            ok = false;
            printf("%s: invalid data at index:%d [got:%u ; expected:%u]\n", fn_name, i, expected_tree[i], flat_tree[i]);
        }
    }
    
    if (PRINT_TREE) {
        for (int i=0; i<expected_data_count; i++) {
            printf("%d ", flat_tree[i]);
        }
        printf("\n");
    }

    printf("%s: ", fn_name);
    if (ok) {
        printf("OK\n");
    }
    else {
        printf("ERROR\n");
    }
    
    // Release memory.
    tree_free(root);
    free(flat_tree);
}

/*
 * Tests definitions start here.
 */


/*
 * Test tree provided by ARM as an example.
 */
void test_assesment_example() {
    const size_t expected_data_count = 9;
    const value_t expected_tree[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    TreeNode *n11 = node_create_with_data(8,3);
    
    TreeNode *n21 = node_create_with_left_data(9);
    TreeNode *n22 = node_create_with_data(6,5);
    TreeNode *n23 = node_create_with_data(2,1);
    
    TreeNode *n31 = node_create_with_left_data(7);
    TreeNode *n32 = node_create_with_right_data(4);
    
    node_set_left_child(n11, n21);
    node_set_mid_child(n11, n22);
    node_set_right_child(n11, n23);
    
    node_set_left_child(n22, n31);
    node_set_right_child(n22, n32);

    test_routine(n11, expected_data_count, expected_tree, __func__);
}

/*
 * Test empty tree.
 */
void test_root_null() {
    const size_t expected_data_count = 0;
    const value_t *expected_tree = NULL;
    
    TreeNode *root = NULL;
    test_routine(root, expected_data_count, expected_tree, __func__);
}

/*
 * Test tree with one node and no data.
 */
void test_root_no_data() {
    const size_t expected_data_count = 0;
    const value_t *expected_tree = NULL;
    
    TreeNode *root = node_create();
    test_routine(root, expected_data_count, expected_tree, __func__);
}

/*
 * Test tree with one node and left data.
 */
void test_root_data_left() {
    const size_t expected_data_count = 1;
    const value_t expected_tree[1] = {13};
    
    TreeNode *root = node_create_with_left_data(13);
    test_routine(root, expected_data_count, expected_tree, __func__);
}

/*
 * Test tree with one node and right data.
 */
void test_root_data_right() {
    const size_t expected_data_count = 1;
    const value_t expected_tree[1] = {3};
    
    TreeNode *root = node_create_with_right_data(3);
    test_routine(root, expected_data_count, expected_tree, __func__);
}

/*
 * Test tree with one node and left and right data.
 */
void test_root_data() {
    const size_t expected_data_count = 2;
    const value_t expected_tree[2] = {0, 32};
    
    TreeNode *root = node_create_with_data(32,0);
    test_routine(root, expected_data_count, expected_tree, __func__);
}

/*
 * Test tree with many nodes and no data.
 */
void test_big_tree_no_data() {
    const size_t expected_data_count = 0;
    const value_t *expected_tree = NULL;
    
    TreeNode *n11 = node_create();
    
    TreeNode *n21 = node_create();
    TreeNode *n22 = node_create();
    TreeNode *n23 = node_create();
    
    TreeNode *n31 = node_create();
    TreeNode *n32 = node_create();
    TreeNode *n33 = node_create();

    TreeNode *n41 = node_create();

    TreeNode *n51 = node_create();
    TreeNode *n52 = node_create();

    node_set_left_child (n11, n21);
    node_set_mid_child  (n11, n22);
    node_set_right_child(n11, n23);
    
    node_set_left_child (n21, n31);
    node_set_mid_child  (n21, n32);
    node_set_right_child(n21, n33);
    
    node_set_left_child (n33, n41);

    node_set_mid_child  (n41, n51);
    node_set_right_child(n41, n52);

    test_routine(n11, expected_data_count, expected_tree, __func__);
}

/*
 * Test tree with many nodes and little data.
 */
void test_big_tree_with_data() {
    const size_t expected_data_count = 2;
    const value_t expected_tree[2] = {2, 1};
    
    TreeNode *n11 = node_create();
    
    TreeNode *n21 = node_create();
    TreeNode *n22 = node_create();
    TreeNode *n23 = node_create_with_right_data(2);
    
    TreeNode *n31 = node_create();
    TreeNode *n32 = node_create();
    TreeNode *n33 = node_create();
    
    TreeNode *n41 = node_create();
    
    TreeNode *n51 = node_create();
    TreeNode *n52 = node_create_with_left_data(1);
    
    node_set_left_child (n11, n21);
    node_set_mid_child  (n11, n22);
    node_set_right_child(n11, n23);
    
    node_set_left_child (n21, n31);
    node_set_mid_child  (n21, n32);
    node_set_right_child(n21, n33);
    
    node_set_left_child (n33, n41);
    
    node_set_mid_child  (n41, n51);
    node_set_right_child(n41, n52);
    
    test_routine(n11, expected_data_count, expected_tree, __func__);
}

/*
 * Test tree with many nodes and plenty of data.
 */
void test_big_tree_full_data() {
    const size_t expected_data_count = 10;
    const value_t expected_tree[10] = {5, 4, 1, 9, 10, 3, 8 , 7, 2, 6};
    
    TreeNode *n11 = node_create_with_left_data(1);
    
    TreeNode *n21 = node_create_with_data(2, 3);
    TreeNode *n22 = node_create_with_right_data(4);
    TreeNode *n23 = node_create_with_left_data(5);
    
    TreeNode *n31 = node_create_with_right_data(6);
    TreeNode *n32 = node_create_with_data(7,8);
    TreeNode *n33 = node_create_with_left_data(9);
    
    TreeNode *n41 = node_create();
    
    TreeNode *n51 = node_create();
    TreeNode *n52 = node_create_with_left_data(10);
    
    node_set_left_child (n11, n21);
    node_set_mid_child  (n11, n22);
    node_set_right_child(n11, n23);
    
    node_set_left_child (n21, n31);
    node_set_mid_child  (n21, n32);
    node_set_right_child(n21, n33);
    
    node_set_left_child (n33, n41);
    
    node_set_mid_child  (n41, n51);
    node_set_right_child(n41, n52);
    
    test_routine(n11, expected_data_count, expected_tree, __func__);
}

/*
 * Function will run all tests.
 */
void run_all_tests() {
    test_assesment_example();
    
    test_root_null();
    test_root_no_data();
    test_root_data_left();
    test_root_data_right();
    test_root_data();
    
    test_big_tree_no_data();
    test_big_tree_with_data();
    test_big_tree_full_data();
}
