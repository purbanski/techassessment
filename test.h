//
//  test.h
//  ARM-tech-assessment
//
//  Created by Przemek Urbanski on 24/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#ifndef test_h
#define test_h

/*
 * Function executes all available tests.
 */
void run_all_tests();

#endif /* test_h */
