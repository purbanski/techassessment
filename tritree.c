//
//  tritree.c
//  ARM-tech-assessment
//
//  Created by Przemek Urbanski on 24/11/16.
//  Copyright © 2016 Przemek Urbanski. All rights reserved.
//

#include <assert.h>
#include <stdlib.h>
#include "tritree.h"

/*
 * Forward declarations.
 */
void tree_travers(value_t *flat_tree, TreeNode *node, size_t *num_elements);
void store_data(value_t *flat_tree, DataNode *data_node, size_t *num_elements);

/*
 * Constant determines size of the buffer, used to store tree data values.
 */
const size_t MAX_TREE_VALUES_COUNT = 100000;

/*
 * Function stores a tree data entry into an array.
 *
 * Args
 *   TreeNode *root - pointer to the root node of the tree.
 *   size_t *num_elements - will get assigned a number of data elements stored in the returned array.
 *
 * Return value
 *   Pointer to an array filled with data elements from the traversed tree.
 */
value_t *flatten(TreeNode *root, size_t *num_elements) {
    assert(num_elements != NULL);
    *num_elements = 0;
    
    // Handle empty tree.
    if (root == NULL) {
        return NULL;
    }
    
    value_t *flat_tree = (value_t*)malloc(MAX_TREE_VALUES_COUNT * sizeof(value_t));
    assert(flat_tree != NULL);
    
    tree_travers(flat_tree, root, num_elements);
    return flat_tree;
}

/*
 * Recrusive function.
 * It travers through a tree in DFS manner. During its traversal,
 * gets the tree nodes data values, and stores them in array flat_tree.
 *
 * Args
 *   value_t *flat_tree - pointer to the array which stores data elements from 
 *                        the traversed tree.
 *   TreeNode *node - pointer to the node to be processed.
 *   size_t *num_elements - current number of data elements stored in flat_tree array.
 *                          Gets incremented each time new data is stored in flat_tree array.
 */
void tree_travers(value_t *flat_tree, TreeNode *node, size_t *num_elements) {

    // I could do realloc here.
    assert(*num_elements < MAX_TREE_VALUES_COUNT);
   
    if (node == NULL)
        return;

    tree_travers(flat_tree, node->right_tree, num_elements);
    store_data(flat_tree, node->right_data, num_elements);    
    
    tree_travers(flat_tree, node->mid_tree, num_elements);

    store_data(flat_tree, node->left_data, num_elements);    
    tree_travers(flat_tree, node->left_tree, num_elements);
}


void store_data(value_t *flat_tree, DataNode *data_node, size_t *num_elements) {
    if (data_node == NULL)
        return;

    flat_tree[*num_elements] = data_node->value;
    (*num_elements)++;
} 
